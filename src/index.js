import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import LandingPage from "./Pages/LandingPage";
import HotelSearch from "./HotelSearch";
import HotelInfo from "./Pages/HotelInfo";
import Loader from "./Components/Loader";
import RoomsPage from "./Pages/RoomsPage";
import BookNow from "./Pages/BookNow";

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route path="/Search" component={HotelSearch} />     
        <Route path="/property" component={HotelInfo} />
        <Route path="/Loader" component={Loader} />
        <Route path="/Rooms" component={RoomsPage} />
        <Route path="/BookNow" component={BookNow} />
      </Switch>
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);
