import React, { Component } from "react";

class PriceFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      priceRange: "",
    };
  }

  getPrice = (event) => {
    this.setState(
      { priceRange: event.target.parentElement.children[1].innerText },
      () => {
        console.log(this.state.priceRange, "Price range updated");
        this.props.setPriceRange(this.state.priceRange);
      }
    );
  };

  render() {
    return (
      <div>
        <section className="mb-4">
          <h6 className="font-weight-bold mb-3">Price</h6>
          <div className="form-check pl-0 mb-3">
            <input
              type="radio"
              className="form-check-input"
              id="under25"
              name="materialExampleRadios"
              onChange={this.getPrice}
            />
            <label
              className="form-check-label small text-uppercase card-link-secondary"
              htmlFor="under25"
            >
              {"\u0024"}1000 to {"\u0024"}2000
            </label>
          </div>
          <div className="form-check pl-0 mb-3">
            <input
              type="radio"
              className="form-check-input"
              id={2550}
              name="materialExampleRadios"
              onChange={this.getPrice}
            />
            <label
              className="form-check-label small text-uppercase card-link-secondary"
              htmlFor={2550}
            >
              {"\u0024"}2000 to {"\u0024"}5000
            </label>
          </div>
          <div className="form-check pl-0 mb-3">
            <input
              type="radio"
              className="form-check-input"
              id={50100}
              name="materialExampleRadios"
              onChange={this.getPrice}
            />
            <label
              className="form-check-label small text-uppercase card-link-secondary"
              htmlFor={50100}
            >
              {"\u0024"}5000 to {"\u0024"}10000
            </label>
          </div>
          <div className="form-check pl-0 mb-3">
            <input
              type="radio"
              className="form-check-input"
              id={100200}
              name="materialExampleRadios"
              onChange={this.getPrice}
            />
            <label
              className="form-check-label small text-uppercase card-link-secondary"
              htmlFor={100200}
            >
              {"\u0024"}10000 to {"\u0024"}20000
            </label>
          </div>
          <div className="form-check pl-0 mb-3">
            <input
              type="radio"
              className="form-check-input"
              id="200above"
              name="materialExampleRadios"
              onChange={this.getPrice}
            />
            <label
              className="form-check-label small text-uppercase card-link-secondary"
              htmlFor="200above"
            >
              {"\u0024"}20000 to {"\u0024"}25000
            </label>
          </div>
        </section>
      </div>
    );
  }
}

export default PriceFilter;
