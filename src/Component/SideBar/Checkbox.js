import React, { Component } from "react";

class Checkbox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ratings: [],
      type: [],
    };
  }

  getRatings = (event) => {
    if (this.props.heading === "User Rating") {
      if (event.target.checked) {
        this.setState(
          (state) => ({
            ratings: [
              ...state.ratings,
              parseInt(event.target.parentElement.children[1].innerText[0]),
            ],
          }),
          () => {
            this.props.setRatings(this.state.ratings);
          }
        );
      } else {
        this.setState(
          (state) => ({
            ratings: state.ratings.filter(
              (rating) =>
                rating !==
                parseInt(event.target.parentElement.children[1].innerText[0])
            ),
          }),
          () => {
            this.props.setRatings(this.state.ratings);
          }
        );
      }
    } else {
      if (event.target.checked) {
        this.setState(
          (state) => ({
            type: [
              ...state.type,
              event.target.parentElement.children[1].innerText,
            ],
          }),
          () => {
            this.props.setType(this.state.type);
          }
        );
      } else {
        this.setState(
          (state) => ({
            type: state.type.filter(
              (typ) => typ !== event.target.parentElement.children[1].innerText
            ),
          }),
          () => {
            this.props.setType(this.state.type);
          }
        );
      }
    }
  };

  render() {
    return (
      <div>
        <section className="mb-4">
          <h6 className="font-weight-bold mb-3">{this.props.heading}</h6>
          <div className="form-check pl-0 mb-3">
            <input
              type="checkbox"
              className="form-check-input filled-in"
              id="new"
              onChange={this.getRatings}
            />
            <label
              className="form-check-label small text-uppercase card-link-secondary"
              htmlFor="new"
            >
              {this.props.content1}
            </label>
          </div>
          <div className="form-check pl-0 mb-3">
            <input
              type="checkbox"
              className="form-check-input filled-in"
              id="used"
              onChange={this.getRatings}
            />
            <label
              className="form-check-label small text-uppercase card-link-secondary"
              htmlFor="used"
            >
              {this.props.content2}
            </label>
          </div>
          <div className="form-check pl-0 mb-3">
            <input
              type="checkbox"
              className="form-check-input filled-in"
              id="collectible"
              onChange={this.getRatings}
            />
            <label
              className="form-check-label small text-uppercase card-link-secondary"
              htmlFor="collectible"
            >
              {this.props.content3}
            </label>
          </div>
        </section>
      </div>
    );
  }
}

export default Checkbox;
