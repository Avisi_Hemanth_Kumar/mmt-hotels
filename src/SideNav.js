import React, { Component } from "react";
import Checkbox from "./Component/SideBar/Checkbox";
import PriceFilter from "./Component/SideBar/PriceFilter";

class SideNav extends Component {
  render() {
    return (
      <div className="container">
        <h5>Select Filters</h5>
        <PriceFilter setPriceRange={this.props.setPriceRange} />
        <Checkbox
          heading={"User Rating"}
          content1={"4 & above (Excellent)"}
          content2={"3 & above (Very Good)"}
          content3={"2 & above (Good)"}
          setRatings={this.props.setRatings}
        />
        <Checkbox
          heading={"Property Type"}
          content1={"Hotel"}
          content2={"Villa"}
          content3={"Apartment"}
          setType={this.props.setType}
        />
      </div>
    );
  }
}

export default SideNav;
