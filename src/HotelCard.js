import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./HotelCard.css";
// const imageName = require("public/Images/hotel1.jpg")
// const imageName = require("public/Images/hotel1.jpg")
const amenitiesData = require("./amenities.json");
const imageName = require("./mainImage.jpg");

class HotelCard extends Component {
  constructor(props) {
    super(props);
    // this.handleClick = this.handleClick.bind(this);
    this.state = {
      // hotelName: "Radisson Blu Plaza Delhi Airport",
      //Ratings:4.5,
      hotelName: this.props.hotelName,
      Ratings: this.props.ratings,
      cityName: this.props.cityName,
      countryCode: this.props.countryCode,
      lines: this.props.lines,
      amenities: this.props.amenities,
      description: this.props.description,
    };
  }

  // handleClick() {
  //   console.log('Click happened');
  // }

  render() {
    console.log(this.state.amenities, "fetched data");
    console.log(amenitiesData, "hard codes");
    return (
      <>
        <Link
          to={{
            pathname: "/property",
            state: {
              hotelName: this.props.hotelName,
              Ratings: this.props.ratings,
              cityName: this.props.cityName,
              countryCode: this.props.countryCode,
              lines: this.props.lines,
              pricing: this.props.priceRange,
              amenities: this.state.amenities,
              description: this.state.description,
            },
          }}
        >
          <div className="outer-card ">
            {/* <div className="container main border border-secondary" onClick={this.handleClick}> */}
            <div className="container main border border-secondary">
              <div className="row">
                <div className="col-sm">
                  <div className="container secondary">
                    <div className="row">
                      <div className="col-4 justify-content-center">
                        {/* <img className="card-image"  src={imageName.default} alt="not found" />  */}
                        <img
                          className="card-image"
                          src={imageName.default}
                          alt="not found"
                        />
                      </div>
                      <div className="col hotel-detail">
                        <h3>{this.state.hotelName}</h3>
                        <h5>{this.state.Ratings || 3}/5</h5>
                        <h6 className="text-muted">
                          {this.state.cityName}
                          {this.state.countryCode}
                          {this.state.lines.join("\n")}
                        </h6>
                        <div className="amenities">
                          <span className="icon-space text-muted">
                            <img
                              className="icons"
                              src={amenitiesData.parking}
                              alt="not found"
                            />
                            parking{" "}
                          </span>
                          <span className="icon-space text-muted">
                            <img
                              className="icons"
                              src={amenitiesData.lounge}
                              alt="not found"
                            />
                            lounge{" "}
                          </span>
                          <span className="icon-space text-muted">
                            <img
                              className="icons"
                              src={amenitiesData.conference}
                              alt="not found"
                            />
                            conference{" "}
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-2 card-price">
                  {"\u0024"} {this.props.priceRange}
                </div>
              </div>
            </div>
          </div>
        </Link>
      </>
    );
  }
}

export default HotelCard;
