import React, { Component } from "react";
import "./HotelSearch.css";
import SideNav from "./SideNav";
import HotelCard from "./HotelCard";
import Header from "./Components/Header";
import api from "./Services/HotelSearchAPI";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";

const month = {
  Jan: 1,
  Feb: 2,
  Mar: 3,
  Apr: 4,
  May: 5,
  Jun: 6,
  Jul: 7,
  Aug: 8,
  Sep: 9,
  Oct: 10,
  Nov: 11,
  Dec: 12,
};

export default class HotelSearch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      place: this.props.location.state.place,
      checkIn: this.format(this.props.location.state.checkIn),
      checkOut: this.format(this.props.location.state.checkOut),
      hotelData: "",
      ratings: [],
      rating: 0,
      originalData: "",
      type: [],
      priceRange: "",
      priceLower: "",
      priceUpper: "",
      rendered: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    console.log(prevState.rating, this.state.rating);
    if (prevState.rating !== this.state.rating) {
      this.setState({ prevState: this.state });
      this.setState(
        (state) => ({
          hotelData: state.originalData.filter((mmt) => {
            return parseInt(mmt.hotel.rating) >= state.rating;
          }),
        }),
        () => {
          console.log(
            this.state.hotelData,
            "updated hotel data",
            this.state.rating
          );
        }
      );
    }

    if (prevState.type.join("") !== this.state.type.join("")) {
      this.setState({ prevState: this.state });
      this.setState(
        (state) => ({
          hotelData: state.originalData.filter((mmt) => {
            return this.state.type.includes(mmt.hotel.type.toUpperCase());
          }),
        }),
        () => {
          console.log(
            this.state.hotelData,
            "updated hotel data",
            this.state.type
          );
        }
      );
    }

    if (prevState.priceRange !== this.state.priceRange) {
      console.log(this.state.priceRange.length - 2, "testing");
      this.setState({ prevState: this.state });
      this.setState(
        (state) => ({
          hotelData: state.originalData.filter((mmt) => {
            return (
              parseInt(mmt.offers[0].price.total * 10) >=
                this.state.priceRange.split(" ")[0].slice(1) &&
              this.state.priceRange
                .split(" ")
                [this.state.priceRange.split(" ").length - 1].slice(1) >=
                parseInt(mmt.offers[0].price.total * 10)
            );
          }),
        }),
        () => {
          console.log(
            this.state.hotelData,
            "updated hotel data",
            this.state.rating
          );
        }
      );
    }
  }

  componentDidMount() {
    api(this.state.place)
      .then((hotels) => {
        this.setState({
          hotelData: hotels.data.data,
          originalData: hotels.data.data,
          rendered: true,
        });
        return this.state.hotelData;
      })
      .catch((err) => {
        console.error(err);
      })
      .then((hotels) => {
        console.log(hotels);
      });
  }

  format = (date) => {
    console.log(`Actual format ${date}`);
    console.log(`Month should be ${month[date.slice(2, 5)]}`);
    console.log(`Year should be 20${date.slice(date.length - 2)}`);
    return `20${date.slice(date.length - 2)}-${
      month[date.slice(2, 5)]
    }-${date}`;
  };

  hotelCards = () => {
    if (this.state.hotelData !== "") {
      return this.state.hotelData.map((mmt) => (
        <HotelCard
          key={mmt.hotel.hotelId}
          hotelName={mmt.hotel.name}
          ratings={mmt.hotel.rating}
          priceRange={mmt.offers[0].price.total * 10}
          cityName={mmt.hotel.address.cityName}
          countryCode={mmt.hotel.address.countryCode}
          lines={mmt.hotel.address.lines}
          amenities={mmt.hotel.amenities}
          description={mmt.hotel.description}
        />
      ));
    }
    return null;
  };

  setPriceRange = (newPrice) => {
    this.setState({ priceRange: newPrice }, () => {
      this.setState({
        priceLower: this.state.priceRange.split(" ")[0].slice(1),
      });
      this.setState({
        priceUpper: this.state.priceRange
          .split(" ")
          [this.state.priceRange.split(" ").length - 1].slice(1),
      });
      console.log(this.state.priceRange, "updated new price range");
    });
  };

  ratingHandle = () => {
    if (this.state.ratings.length === 0) {
      this.setState({ rating: 0 });
    } else {
      this.setState(
        (state) => ({
          rating: state.ratings.reduce((a, b) => Math.min(a, b)),
        }),
        () => {
          console.log(this.state.rating);
        }
      );
    }
  };

  setRatings = (newRatings) => {
    this.setState({ ratings: newRatings }, () => {
      console.log(this.state.ratings, "updated new Ratings");
      this.ratingHandle();
    });
  };

  setType = (newType) => {
    this.setState({ type: newType }, () => {
      console.log(this.state.type, "updated new Type");
    });
  };

  loader = () => {
    return (
      <div id="loader">
        <Loader
          type="Puff"
          color="#00BFFF"
          height={100}
          width={100}
          timeout={500000} //
          className="loaderSize"
        />
        <div id="loadText">
          <strong>Loading Hotels...</strong>
        </div>
      </div>
    );
  };

  render() {
    if (this.state.rendered === false) {
      return this.loader();
    } else {
      return (
        <div>
          <Header />
          <div className="container">
            <div className="row">
              <div className="col-3">
                <SideNav
                  setPriceRange={this.setPriceRange}
                  setRatings={this.setRatings}
                  setType={this.setType}
                />
              </div>
              <div className="col">
                {this.state.rendered === true
                  ? this.hotelCards()
                  : this.loader()}
                <div></div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}
