import React, { Component } from "react";
import Bookings from "./Components/Bookings";
import FormDetail from "./Components/FormDetail";
import PopUp from "./Components/PopUp";
import TotalFair from "./Components/TotalFair";


export default class ReviewContainer extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.state = {
          name:"xyz"
      };
      }
    


 handleClick(){
     {{console.log("click happened")}}
       <PopUp />
      }
    
  render() {
    return (
      <div>

        <div className="container">
        <h1 className="ReviewTitle">Review your Booking </h1>
          <div className="row">
            <div className="col-8">
               
              <div className="ReviewContainer1">
              <Bookings />
              </div>  
              
              <FormDetail />
              <button className ="btn" type="button" class="btn btn-primary" onClick={this.handleClick}>Pay Now</button> 
 
            </div>
            <div className="col-4">
            <div className="ReviewContainer2">
              <TotalFair />
                
            </div>  
             
            </div>
          </div>
        </div>
      </div>
    );
  }
}
