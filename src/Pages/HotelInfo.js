import React, { Component } from "react";
import AboutHotel from "../Components/AboutHotel";
import DetailCard from "../Components/DetailCard";
import Header from "../Components/Header";
import RestAssured from "../Components/RestAssured";
import Facilities from "../Components/Facilities";

import "./HotelInfo.css";

export default class HotelInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hotelName: this.props.location.state.hotelName,
      Ratings: this.props.location.state.ratings,
      cityName: this.props.location.state.cityName,
      countryCode: this.props.location.state.countryCode,
      lines: this.props.location.state.lines,
      pricing: this.props.location.state.pricing,
      amenities: this.props.location.state.amenities,
      description: this.props.location.state.description,
    };
  }

  render() {
    console.log(this.state.pricing, "Hiiii byee");
    return (
      <div>
        <Header />
        <div className="container">
          <DetailCard priceRange={this.state.pricing} />
          <div className="hotelBrief">
            <h1 className="hotelHeading">
              {this.state.hotelName}
              <i className="fas fa-star"></i>
              <i className="fas fa-star"></i>
              <i className="fas fa-star"></i>
              <i className="fas fa-star"></i>
              <i className="fas fa-star"></i>
            </h1>

            <h6>
              {this.state.cityName} {this.state.countryCode}{" "}
              {this.state.lines.join("\n")}
            </h6>
          </div>
          <RestAssured />
          <AboutHotel
            hotelName={this.state.hotelName}
            cityName={this.state.cityName}
            countryCod={this.state.countryCode}
            lines={this.state.lines}
            pricing={this.state.pricing}
            amenities={this.state.amenities}
            description= {this.state.description}
          />
          <Facilities />
        </div>
      </div>
    );
  }
}
