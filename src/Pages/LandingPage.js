import React, { Component } from "react";
import Header from "../Components/Header";
import Search from "../Components/Search";
import "../LandingPage.css";

export default class LandingPage extends Component {
  render() {
    const myStyle = {
      backgroundImage:
        "linear-gradient(rgba(2, 0, 36, 1) 0%,rgba(9, 9, 121, 1) 35%,rgba(0, 212, 255, 1) 100%)",
      backgroundRepeat: "no-repeat",
      backgroundSize: "100% 100vh",
    };

    return (
      <>
        <div style={myStyle}>
          <Header />
          <Search />
        </div>
        <div className="w-100">
          <div className="col"></div>
        </div>
      </>
    );
  }
}
