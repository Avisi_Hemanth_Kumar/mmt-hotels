import React, { Component } from "react";
import Header from "../Components/Header";
import "../RoomsPage.css";


const imageName2 = require('../image2.webp')

export default class RoomsPage extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container">
        <h1 className="RoomHeading">Room Type</h1>
        <div className="cards">
          <div className="card">
            <header>
              <h2>Day Use Room </h2>
            </header>
            <img
              src={imageName2.default}
              alt="Bristol harbour"
              className="RoomImage"
            />
            <div className="body">
              <p>
                <li>
                  Same day check-out by 6 PM for day use room (Overnight stay
                  not applicable). Extension beyond 6 PM can be done on
                  additional charges.
                </li>
                <li>9am to 6 pm stay </li>
                <li>No meals included</li>
                <li>Price</li>
                <a href="/BookNow">
                  <button className="btn" type="button" class="btn btn-primary">
                    Book this now
                  </button>
                </a>
              </p>
            </div>
          </div>
          <div className="card">
            <header>
              <h2>Superior Room</h2>
            </header>
            <img
              src={imageName2.default}
              alt="Bristol harbour"
              className="RoomImage"

            />
            <div className="body">
            <p>
                <li>
                Complimentary Breakfast is available.
                </li>

                <li>Complimentary Lunch is available.</li>
                <li>Complimentary Early Check </li>
                <li>Complimentary Room Upgrade is available. </li>
                <li>Price</li>
                <a href="/BookNow">
                  <button className="btn" type="button" class="btn btn-primary">
                    Book this now
                  </button>
                </a>
              </p>
            </div>
          </div>
          <div className="card">
            <header>
              <h2>Executive Suite</h2>
            </header>
            <img
                src={imageName2.default}
              alt="Coloured balloon"
              className="RoomImage"
            />
            <div className="body">
            <p>
                <li>
                Complimentary Breakfast is available.

                </li>

                <li>Complimentary Dinner is available.</li>
                <li>
                Complimentary Room Upgrade is available. This service is subject to availability.
                </li>
                <li>Price</li>
                <a href="/BookNow">
                  <button className="btn" type="button" class="btn btn-primary">
                    Book this now
                  </button>
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
}
