import axios from "axios";
import qs from "qs";

const data = qs.stringify({
  client_id: process.env.REACT_APP_API_KEY,
  client_secret: process.env.REACT_APP_API_SECRET,
  grant_type: "client_credentials",
});

const security = {
  method: "post",
  url: "https://test.api.amadeus.com/v1/security/oauth2/token",
  headers: {},
  data: data,
};

function api(place) {
  const hotels = {
    method: "get",
    url: `https://test.api.amadeus.com/v2/shopping/hotel-offers?cityCode=${place}`,
    headers: {},
  };

  return axios(security)
    .then((response) => {
      return response.data.access_token;
    })
    .catch((error) => {
      console.log(error);
    })
    .then((token) => {
      hotels.headers.Authorization = `Bearer ${token}`;
      return hotels;
    })
    .then((result) => {
      return axios(result);
    });
}
export default api;
