import React, { Component } from "react";
import DatePicker from "./DatePicker";

export default class SearchCheckIn extends Component {
  render() {
    return (
      <>
        <DatePicker type="CHECK-IN" role={this.props.checkIn} />
      </>
    );
  }
}
