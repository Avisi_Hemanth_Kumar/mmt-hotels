import React, { Component } from "react";

export default class RoomsGuests extends Component {
  handleChange = (event) => {
    this.props.changeCount(event.target.value, event.target.value);
  };

  render() {
    return (
      <div className="card container" id='Rooms'>
        <div className="card listContainer">
          <div className="card">Select Rooms</div>
          <div className="card checkContainer">
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox1"
                value="1"
              />
              <label className="form-check-label" for="inlineCheckbox1">
                1
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                2
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                3
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                4
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                5
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                6
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                7
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                8
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                9
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                10
              </label>
            </div>
          </div>
        </div>
        <div className="card listContainer">
          <div className="card">Select Guests</div>
          <div className="card checkContainer">
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox1"
                value="1"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox1">
                1
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                2
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                3
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                4
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                5
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                6
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                7
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                8
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                9
              </label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="checkbox"
                id="inlineCheckbox2"
                value="2"
                onChange={this.handleChange}
              />
              <label className="form-check-label" for="inlineCheckbox2">
                10
              </label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
