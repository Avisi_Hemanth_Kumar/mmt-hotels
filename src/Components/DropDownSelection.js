import React, { Component } from "react";

export default class DropDownSelection extends Component {
  constructor(props) {
    super(props);
    this.state = { place: this.props.place, isOverLayed: false };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ place: event.target.value });
  }

  handleSubmit(event) {
    console.log("A name was submitted: " + this.state.place);
    event.preventDefault();
    this.setState({ isOverLayed: true });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          <div>
            <div>
              <div className="action">{this.props.type}</div>
              <h3>{this.props.place}</h3>
              <div className="placeRooms">
                <div className="displayRooms">
                  <h3>{this.props.rooms}</h3>{" "}
                  <div className="country">{this.props.Rooms}</div>
                </div>
                <div className="displayRooms">
                  <h3>{this.props.guests}</h3>{" "}
                  <div className="country">{this.props.Guests}</div>
                </div>
              </div>
              <div className="country">{this.props.country}</div>
            </div>
          </div>
        </label>
      </form>
    );
  }
}
