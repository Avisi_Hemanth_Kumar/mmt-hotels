import React, { Component } from "react";

export default class TotalFair extends Component {
  render() {
    return (
      <div>
        <div className="container border border-light priceContainer">
          <div className="row">
            <div className="col priceSpace"><h5 className="PriceTitle">PRICE BREAK-UP</h5></div>
            <div className="w-100 border-bottom  " />
            <div className="col priceSpace">
                <span className="left">1 Room x 1 Night</span>
                <span className="right">$ 7,499</span>
            </div>
            <div className="w-100 border-bottom  " />
            <div className="col priceSpace ">
            <span className="left">Total Discount</span>
            <span className="right">$ 1000</span>
            </div>
            <div className="w-100 border-bottom " />
            <div className="col priceSpace ">
            <span className="left">Total Price:</span>
            <span className="right">$ 6499</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
