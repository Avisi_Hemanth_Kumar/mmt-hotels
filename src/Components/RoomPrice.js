import React, { Component } from "react";
import "./RoomPrice.css";

export default class RoomPrice extends Component {
  constructor(props) {
    super(props);

    this.state = {
      priceRange: this.props.priceRange,
    };
  }

  render() {
    return (
      <div>
        <div className="container roomPrice">
          <div className="row">
            <div className="col rooms">
              <h4>Standard Twin Room</h4>
              <h6 className="textColor">Per Night</h6>
              <h6 className="textColor">For 2 Adults</h6>
              <div>
                <h6 className="textColor">Non-Refundable</h6>
                <h6 className="textColor">Room Only</h6>
              </div>
            </div>
            <div className="col price">
              <h4>$ {this.state.priceRange}</h4>
              <h6 className="textColor">+ $ {this.state.priceRange / 10}</h6>
              <h6 className="textColor">taxes &amp; fees</h6>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
