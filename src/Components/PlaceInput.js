import React, { Component } from "react";

export default class PlaceInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      place: "",
      placeRequest: false,
    };
  }

  handleChange = (event) => {
    this.setState({ place: event.target.value });
  };

  clearCache = () => {
    this.setState({ place: "" });
  };

  handleSubmit = (event) => {
    console.log("A name was submitted: " + this.state.place);
    this.clearCache();
    this.props.changePlace(this.state.place);
    event.preventDefault();
  };

  render() {
    if (this.state.place.length === 0 || this.state.placeRequest === false) {
      console.log("came to render");
      return (
        <div className="Input card">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <input
                type="text"
                value={this.state.place}
                onChange={this.handleChange}
                className="form-control"
                id="formGroupExampleInput"
                placeholder="Enter CITY / HOTEL / AREA / BUILDING"
              />
              <input type="submit" value="Submit" />
            </div>
          </form>
        </div>
      );
    } else {
      return null;
    }
  }
}
