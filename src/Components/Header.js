import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <>
        <header className="sticky-top headerTop">
          <div className="fluid-container header">
            <a href="/" className="navbar-brand">
              <img
                src="//imgak.mmtcdn.com/pwa_v3/pwa_hotel_assets/header/logo@2x.png"
                alt="MMT LOGO"
              />
            </a>

            <a href="/">
              <span className="span">
                <span>
                  <i className="fas fa-hotel"></i>
                </span>
                {"  "}
                <span>
                  <strong>Hotels</strong>
                </span>
              </span>
            </a>
          </div>
        </header>
      </>
    );
  }
}
