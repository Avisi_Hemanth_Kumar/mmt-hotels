import React, { Component } from "react";
import PassangerDetail from "./PassangerDetail";

export default class Bookings extends Component {
  render() {
    return (
      <div>
        <>
  {/*Section: Accordion*/}
  <section className="mb-5">
    {/*Accordion wrapper*/}
    <div
      className="md-accordion accordion "
      id="accordionEx"
      role="tablist"
      aria-multiselectable="true"
    >
      {/* Accordion card */}
      <div className="card">
        {/* Card header */}
        <div className="card-header" role="tab" id="headingOne">
          <a
            data-toggle="collapse"
            data-parent="#accordionEx"
            href="#collapseOne"
            aria-expanded="true"
            aria-controls="collapseOne"
          >
            <h5 className="mb-0">
             Hotel Information
              <i className="fas fa-angle-down rotate-icon downArrow" />
            </h5>
          </a>
        </div>
        {/* Card body */}
        <div
          id="collapseOne"
          className="collapse show"
          role="tabpanel"
          aria-labelledby="headingOne"
        >
          <div className="card-body">
           <h3 className="hotelName">Lemon Tree Premier Delhi Airport</h3>
           <h6 className="Location">Asset No. 6, Aerocity Hospitality District,IGI Airport , Delhi, India</h6>   
          <PassangerDetail />
          </div>
        </div>
      </div>
      {/* Accordion card */}
      {/* Accordion card */}
    </div>
    {/*/.Accordion wrapper*/}
  </section>
  {/*Section: Accordion*/}
</>

     </div>
    );
  }
}
