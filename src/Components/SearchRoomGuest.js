import React, { Component } from "react";
import DropDownSelection from "./DropDownSelection";

export default class SearchRoomGuest extends Component {
  render() {
    return (
      <>
        <DropDownSelection
          type="ROOMS & GUESTS"
          option1={1}
          option2={2}
          option3={3}
          option4={4}
          option5={5}
          rooms={this.props.rooms}
          Rooms="Rooms"
          guests={this.props.guests}
          Guests="Guests"
        />
        <DropDownSelection
          type=""
          option1={1}
          option2={2}
          option3={3}
          option4={4}
          option5={5}
        />
      </>
    );
  }
}
