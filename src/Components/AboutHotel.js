import React, { Component } from "react";

export default class AboutHotel extends Component {
  getAmenities = () => {
    return (
      <ul>
        <li className="AboutList">Facility for {this.props.amenities[0]}</li>
        <li className="AboutList">Facility for {this.props.amenities[1]}</li>
        <li className="AboutList">Facility for {this.props.amenities[2]}</li>
        <li className="AboutList">Facility for {this.props.amenities[3]}</li>
      </ul>
    );
  };
  getDescription = () => {
    return <h5 className="location">{this.props.description.text}</h5>;
  };
  render() {
    console.log(this.props.description.text, 'desc');
    return (
      <div className="AboutHotel-Cont">
        <h4 className="Heading">
          About {this.props.hotelName}
          {"-"}
          {this.props.cityName} {this.props.lines}{" "}
        </h4>
        {/* <h5 className="checkin">Checkin and check out</h5> */}
        {this.props.description.text !== undefined ? this.getDescription() : null}
        {this.props.amenities !== undefined ? this.getAmenities() : null}
      </div>
    );
  }
}
