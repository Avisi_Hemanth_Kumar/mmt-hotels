import React, { Component } from "react";
import PlaceInput from "./PlaceInput";
import SearchCheckIn from "./SearchCheckIn";
import SearchCheckOut from "./SearchCheckOut";
import SearchPlace from "./SearchPlace";
import SearchRoomGuest from "./SearchRoomGuest";
import CheckInInput from "./CheckInInput";
import CheckOutInput from "./CheckOutInput";
import RoomsGuests from "./RoomsGuests";
import Header from "../Components/Header";
import "../Search2.css";
export default class Search2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      placeRequest: false,
      checkInRequest: false,
      checkOutRequest: false,
      roomGuestRequest: false,
    };
  }

  placeHandler = () => {
    console.log("clicked");
    this.setState({ placeRequest: true }, () => {
      console.log(this.state.placeRequest);
    });
  };

  checkInHandler = () => {
    console.log("clicked");
    this.setState({ checkInRequest: true }, () => {
      console.log(this.state.checkInRequest);
    });
  };

  checkOutHandler = () => {
    console.log("clicked");
    this.setState({ checkOutRequest: true }, () => {
      console.log(this.state.checkOutRequest);
    });
  };

  roomGuestHandler = () => {
    console.log("inside room guest handler");
    this.setState({ roomGuestRequest: true }, () => {
      console.log("set room guest request", this.state.roomGuestRequest);
    });
  };

  placeRender = () => {
    console.log("came inside placeRender");
    if (this.state.placeRequest === true) {
      console.log("came inside return for place input");
      return <PlaceInput changeState={this.changePlaceRequest} />;
    }
  };

  checkInRender = () => {
    if (this.state.checkInRequest === true) {
      console.log("came inside return for place input");
      return <CheckInInput changeState={this.changeCheckInRequest} />;
    }
  };

  checkOutRender = () => {
    if (this.state.checkOutRequest === true) {
      console.log("came inside return for place input");
      return <CheckOutInput changeState={this.changeCheckOutRequest} />;
    }
  };

  roomGuestRender = () => {
    if (this.state.roomGuestRequest === true) {
      console.log("came inside room guest render");
      return <RoomsGuests changeState={this.changeRoomGuestRequest} />;
    }
  };

  changePlaceRequest = () => {
    this.setState({ placeRequest: false }, () => {
      console.log("changed", this.state.placeRequest);
    });
  };

  changeCheckInRequest = () => {
    this.setState({ checkInRequest: false }, () => {
      console.log("changed", this.state.checkInRequest);
    });
  };

  changeCheckOutRequest = () => {
    this.setState({ checkOutRequest: false }, () => {
      console.log("changed", this.state.checkOutRequest);
    });
  };

  changeRoomGuestRequest = () => {
    this.setState({ roomGuestRequest: false }, () => {
      console.log("changed room guest request", this.state.roomGuestRequest);
    });
  };

  render() {
    return (
      <>
        <Header />
        <div className="fluid-container searchContainer">
          <div className="row fluid-container inputContainer">
            <div className="cardContainer">
              <div className="card cardPlace" onClick={this.placeHandler}>
                <SearchPlace />
              </div>
              <div className="card cardCheck" onClick={this.checkInHandler}>
                <SearchCheckIn />
              </div>
              <div className="card cardCheck" onClick={this.checkOutHandler}>
                <SearchCheckOut />
              </div>
              <div className="card cardType" onClick={this.roomGuestHandler}>
                <SearchRoomGuest />
              </div>
            </div>
          </div>
          <div className="row justify-content-md-center w-25">
            <button type="button" className="btn btn-primary button">
              <strong>
                <a href="/Search">SEARCH</a>
              </strong>
            </button>
          </div>
        </div>
        <div>{this.placeRender()}</div>
        <div>{this.checkInRender()}</div>
        <div>{this.checkOutRender()}</div>
        <div>{this.roomGuestRender()}</div>
      </>
    );
  }
}
