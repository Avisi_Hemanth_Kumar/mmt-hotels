import React, { Component } from 'react'
import "./Carousel.css"
const imageName1 = require('../hotel1.jpg')
const imageName2 = require('../image2.webp')
const imageName3 = require('../hotel3.jpg')
// <img className="card-image"  src={imageName.default} alt="not found" /> 

export default class Carousel extends Component {
    render() {
        return (
            <div>
    
            <div
              id="carouselExampleControls"
              className="carousel slide"
              data-ride="carousel"
            >
              <div className="carousel-inner">
                <div className="carousel-item active">
                  <img className="d-block w-100" src={imageName1.default} alt="First slide" />
                </div>
                <div className="carousel-item">
                  <img className="d-block w-100" src={imageName2.default}  alt="Second slide" />
                </div>
                <div className="carousel-item">
                  <img className="d-block w-100" src={imageName3.default}  alt="Third slide" />
                </div>
              </div>
              <a
                className="carousel-control-prev"
                href="#carouselExampleControls"
                role="button"
                data-slide="prev"
              >
                <span className="carousel-control-prev-icon" aria-hidden="true" />
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="carousel-control-next"
                href="#carouselExampleControls"
                role="button"
                data-slide="next"
              >
                <span className="carousel-control-next-icon" aria-hidden="true" />
                <span className="sr-only">Next</span>
              </a>
            </div>
          </div>
          
        )
    }
}
