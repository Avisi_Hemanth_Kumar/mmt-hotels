import React, { Component } from "react";
import Carousel from "./Carousel";
import "./DetailCard.css";
import RoomPrice from "./RoomPrice";

export default class DetailCard extends Component {
  render() {
    return (
      <div>
        <div class="container  detailCard">
          <div class="row ">
            <div class="col-md">
              <Carousel />
            </div>
            <div class="col-4">
              <div class="container border border-2 subCard">
                <h1>
                  <RoomPrice priceRange={this.props.priceRange} />
                </h1>
              </div>
              <a href="/Rooms">
                <button className="btn" type="button" class="btn btn-primary">
                  Rooms
                </button>
              </a>
              <a href="/BookNow">
                <button className="btn" type="button" class="btn btn-primary">
                  Book this now
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
