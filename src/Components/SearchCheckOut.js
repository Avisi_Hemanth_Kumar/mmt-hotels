import React, { Component } from "react";
import DatePicker from "./DatePicker";

export default class SearchCheckOut extends Component {
  render() {
    return (
      <>
        <DatePicker type="CHECK-OUT" role={this.props.checkOut} />
      </>
    );
  }
}
