import React, { Component } from "react";

export default class CheckInInput extends Component {
  handleChange = (event) => {
    this.props.changeCheckIn(event.target.value);
  };

  render() {
    return (
      <div className="CheckIn card">
        <input type="date" onChange={this.handleChange} />
      </div>
    );
  }
}
