import React, { Component } from "react";

export default class RestAssured extends Component {
  render() {
    return (
      <div className="assuredBox">
        <div
          className="container border border-secondary assuredContainer"
          style={{ "border-radius": "0.3em" }}
        >
          <div className="row">
            <div className="col assuredTitle">
              <h3>ALLSAFE</h3>
              <p className="greyText">
                ALLSAFE label verified by Bureau Veritas represents elevated
                cleanliness standards with assurance that these standards are
                met in our hotels
              </p>
            </div>
            <div className="w-100 border-top border-secondary" />
            <div className="col assuredDetails">
              <ul>
                <li>
                  <img className="tick"
                    src="http://promos.makemytrip.com/COVID/GreenCircular.png"
                    alt
                  />Strengthened room cleaning protocols including extra disinfection of all high touch room areas
                </li>
                <li>
                  <img className="tick"
                    src="http://promos.makemytrip.com/COVID/GreenCircular.png"
                    alt
                  />Reinforced cleaning program in public areas with frequent disinfection of all high touch areas
                </li>
                <li>
                  <img className="tick"
                    src="http://promos.makemytrip.com/COVID/GreenCircular.png"
                    alt
                  />Guest access to medical professionals and tele- medical support

                </li>
                <li>
                  <img className="tick"
                    src="http://promos.makemytrip.com/COVID/GreenCircular.png"
                    alt
                  />ALLSAFE Officer appointed across all Accor hotels, responsible for guest health and wellbeing
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
