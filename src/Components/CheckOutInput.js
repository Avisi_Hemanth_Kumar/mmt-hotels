import React, { Component } from "react";

export default class CheckOutInput extends Component {

  handleChange = (event) => {
    this.props.changeCheckOut(event.target.value);
  };

  render() {
    return (
      <div className="CheckOut card">
        <input type="date" onChange={this.handleChange} />
      </div>
    );
  }
}
