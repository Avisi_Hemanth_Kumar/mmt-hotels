import React, { Component } from "react";
import DropDownSelection from "./DropDownSelection";

export default class SearchPlace extends Component {
  render() {
    return (
      <>
        <DropDownSelection
          type="CITY / HOTEL / AREA / BUILDING"
          option1="Hyderabad"
          option2="Pune"
          option3="Chennai"
          option4="Delhi"
          option5="Bangalore"
          place={this.props.place}
          country="India"
        />
      </>
    );
  }
}
