import React, { Component } from "react";

export default class DatePicker extends Component {
  constructor(props) {
    super(props);
    this.day = {
      1: "Monday",
      2: "Tuesday",
      3: "Wednesday",
      4: "Thursday",
      5: "Friday",
      6: "Saturday",
      7: "Sunday",
    };
    this.month = {
      0: "Jan'",
      1: "Feb'",
      2: "Mar'",
      3: "Apr'",
      4: "May'",
      5: "Jun'",
      6: "Jul'",
      7: "Aug'",
      8: "Sep'",
      9: "Oct'",
      10: "Nov'",
      11: "Dec'",
    };
    this.state = { value: "coconut", date: new Date() };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <div>
          <div className="action">{this.props.type}</div>
        </div>
        <div className="displayDate">
          <h3>{this.props.role.slice(this.props.role.length - 2)}</h3>
          <div className="displayAlign">
            {this.month[this.state.date.getMonth()]}
            {this.state.date.getUTCFullYear().toLocaleString().slice(3)}
          </div>
        </div>
        <div className="country">{this.day[this.state.date.getDay()]}</div>
      </div>
    );
  }
}
