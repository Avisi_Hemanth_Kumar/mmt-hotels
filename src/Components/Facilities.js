import React, { Component } from "react";
const amenitiesData = require('../amenities.json')

export default class Facilities extends Component {
  render() {
    return (
      <div>
        <h4 className="Heading">
          Facilities at Lemon Tree Premier Delhi Airport
        </h4>
        {/*Container grid with all 5 facilties shown with icon*/}
        <div className="container border border-secondary facility-inner">
          <div className="row">

          <div className="amenities">
            <span className="icon-spacePage text-muted facility-icon"><img className= "icons " src={amenitiesData.parking} alt="not found" />Parking </span>
            <span className="icon-spacePage text-muted facility-icon"><img className= "icons " src={amenitiesData.lounge} alt="not found" />Lounge </span>
            <span className="icon-spacePage text-muted facility-icon"><img className= "icons " src={amenitiesData.conference} alt="not found" />Conference </span>
            <span className="icon-spacePage text-muted facility-icon"><img className= "icons " src={amenitiesData.yoga} alt="not found" />Yoga </span>
          </div>
                
              </div>


           
        </div>
      </div>
    );
  }
}
