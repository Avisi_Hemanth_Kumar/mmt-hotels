import React, { Component } from "react";

export default class Loader extends Component {
  render() {
    return (
      <div>
        <div
          className="spinner-border"
          style={{ width: "3rem", height: "3rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }
}
