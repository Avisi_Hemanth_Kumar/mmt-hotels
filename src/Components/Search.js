import React, { Component } from "react";
import { Link } from "react-router-dom";

import PlaceInput from "./PlaceInput";
import SearchCheckIn from "./SearchCheckIn";
import SearchCheckOut from "./SearchCheckOut";
import SearchPlace from "./SearchPlace";
import SearchRoomGuest from "./SearchRoomGuest";
import CheckInInput from "./CheckInInput";
import CheckOutInput from "./CheckOutInput";
import RoomsGuests from "./RoomsGuests";

export default class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      placeRequest: false,
      checkInRequest: false,
      checkOutRequest: false,
      roomGuestRequest: false,
      place: "PAR",
      checkIn: "22Jul'21",
      checkOut: "23Jul'21",
      rooms: 2,
      guests: 6,
    };
  }

  placeHandler = () => {
    console.log("clicked");
    this.setState({ placeRequest: true }, () => {
      console.log(this.state.placeRequest);
    });
  };

  checkInHandler = () => {
    console.log("clicked");
    this.setState({ checkInRequest: true }, () => {
      console.log(this.state.checkInRequest);
    });
  };

  checkOutHandler = () => {
    console.log("clicked");
    this.setState({ checkOutRequest: true }, () => {
      console.log(this.state.checkOutRequest);
    });
  };

  roomGuestHandler = () => {
    console.log("inside room guest handler");
    this.setState({ roomGuestRequest: true }, () => {
      console.log("set room guest request", this.state.roomGuestRequest);
    });
  };

  placeRender = () => {
    console.log("came inside placeRender");
    if (this.state.placeRequest === true) {
      console.log("came inside return for place input");
      return <PlaceInput changePlace={this.changePlace} />;
    }
  };

  checkInRender = () => {
    if (this.state.checkInRequest === true) {
      console.log("came inside return for place input");
      return <CheckInInput changeCheckIn={this.changeCheckIn} />;
    }
  };

  checkOutRender = () => {
    if (this.state.checkOutRequest === true) {
      console.log("came inside return for place input");
      return <CheckOutInput changeCheckOut={this.changeCheckOut} />;
    }
  };

  roomGuestRender = () => {
    if (this.state.roomGuestRequest === true) {
      console.log("came inside room guest render");
      return <RoomsGuests changeCount={this.changeCount} />;
    }
  };

  changePlaceRequest = () => {
    this.setState({ placeRequest: false }, () => {
      console.log("changed", this.state.placeRequest);
    });
  };

  changeCheckInRequest = () => {
    this.setState({ checkInRequest: false }, () => {
      console.log("changed", this.state.checkInRequest);
    });
  };

  changeCheckOutRequest = () => {
    this.setState({ checkOutRequest: false }, () => {
      console.log("changed", this.state.checkOutRequest);
    });
  };

  changeRoomGuestRequest = () => {
    this.setState({ roomGuestRequest: false }, () => {
      console.log("changed room guest request", this.state.roomGuestRequest);
    });
  };

  changePlace = (newPlace) => {
    this.setState({ place: newPlace, placeRequest: false }, () => {
      console.log("place changes", this.state.place);
    });
  };

  changeCheckIn = (newCheckIn) => {
    console.log(newCheckIn);
    this.setState({ checkIn: newCheckIn, checkInRequest: false }, () => {
      console.log("checkIn changes", this.state.checkIn);
    });
  };

  changeCheckOut = (newCheckOut) => {
    this.setState({ checkOut: newCheckOut, checkOutRequest: false }, () => {
      console.log("checkout changes", this.state.checkOut);
    });
  };

  changeCount = (newRooms, newGuests) => {
    this.setState(
      { rooms: newRooms, guests: newGuests, roomGuestRequest: false },
      () => {
        console.log("place changes", this.state.newRooms);
      }
    );
  };

  render() {
    return (
      <div className="searchOuter">
        <div className="fluid-container searchContainer">
          <div className="row">
            <strong className="action">Book International Hotels</strong>
          </div>
          <div className="row fluid-container inputContainer">
            <div className="cardContainer">
              <div className="card cardPlace" onClick={this.placeHandler}>
                <SearchPlace place={this.state.place} />
              </div>
              <div className="card cardCheck" onClick={this.checkInHandler}>
                <SearchCheckIn checkIn={this.state.checkIn} />
              </div>
              <div className="card cardCheck" onClick={this.checkOutHandler}>
                <SearchCheckOut checkOut={this.state.checkOut} />
              </div>
              <div className="card cardType" onClick={this.roomGuestHandler}>
                <SearchRoomGuest
                  rooms={this.state.rooms}
                  guests={this.state.guests}
                />
              </div>
            </div>
          </div>
          <div className="row justify-content-md-center w-25">
            <button type="button" className="btn btn-primary button">
              <strong>
                <Link
                  to={{
                    pathname: "/Search",
                    state: {
                      place: this.state.place,
                      checkIn: this.state.checkIn,
                      checkOut: this.state.checkOut,
                    },
                  }}
                >
                  SEARCH
                </Link>
              </strong>
            </button>
          </div>
        </div >
        <div>{this.placeRender()}</div>
        <div>{this.checkInRender()}</div>
        <div>{this.checkOutRender()}</div>
        <div>{this.roomGuestRender()}</div>
      </div>
    );
  }
}
