import React, { Component } from "react";
import SearchCheckIn from "./SearchCheckIn";
import SearchCheckOut from "./SearchCheckOut";
import SearchRoomGuest from "./SearchRoomGuest";


export default class PassangerDetail extends Component {
  render() {
    return (
      <div className="passenger-Outer border ">
        <div className="container">
          <div className="row">
            <div className="col-3">
                <div>
                    <SearchCheckIn />
                </div>
            </div>
            <div className="col-2">
            <h1>&#8596;</h1>
            </div>
            <div className="col-3">
                <div>
                <SearchCheckOut />
                </div>
                </div>
            <div className="col-3"><div>
            <SearchRoomGuest />
                </div>
                </div>
          </div>
        </div>
      </div>    
    );
  }
}
