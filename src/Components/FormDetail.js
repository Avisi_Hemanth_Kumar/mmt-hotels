import React, { Component } from "react";
import Form from "./Form";

export default class FormDetail extends Component {
  render() {
    return (
      <div >
        <>
          {/*Section: Accordion*/}
          <section className="mb-5">
            {/*Accordion wrapper*/}
            <div
              className="md-accordion accordion"
              id="accordionEx"
              role="tablist"
              aria-multiselectable="true"
            >
              {/* Accordion card */}
              <div className="card">
                {/* Card header */}
                <div className="card-header" role="tab" id="headingOne">
                  <a
                    data-toggle="collapse"
                    data-parent="#accordionEx"
                    href="#collapseOne"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                  
                    
                  >
                    <h5 className="mb-0">
                      Checkout
                      <i className="fas fa-angle-down rotate-icon downArrow1" />
                    </h5>
                  </a>
                </div>
                {/* Card body */}
                <div
                  id="collapseOne"
                  className="collapse show"
                  role="tabpanel"
                  aria-labelledby="headingOne"
                >
                  <div className="card-body formBox">
                   <h1><Form /> </h1>
                  </div>
                </div>
              </div>
              {/* Accordion card */}
              {/* Accordion card */}
            </div>
            {/*/.Accordion wrapper*/}
          </section>
          {/*Section: Accordion*/}
        </>
      </div>
    );
  }
}
